#!/bin/bash

# Hopefully we will read some parameters for this script from the command line.

echo "To use: ./ArgumentsExample.sh Thing1 Thing2 Thing3"

POSPAR1="$1"
PASPAR2="$2"
PASPAR3="$3"

echo "$1 is the first position parameter. \$1."
echo "$2 is the second position parameter. \$2."
echo "$3 is the third position parameter. \$3."
echo 
echo "The total number of parameters is given by $#."
