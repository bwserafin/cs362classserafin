#!/bin/bash

HEIGHT=15
WIDTH=40
CHOICE_HEIGHT=4
BACKTITLE="Your Options Back Title"
TITLE="Big Time Options"
MENU="You Must Choose!"

OPTIONS=(1 "Option 1"
	2 "Option 2"
	3 "Option 3")

CHOICE=$(dialog --clear  \
		--backtitle "$BACKTITLE" \
		--title "$TITLE" \
		--menu "$MENU" \
		$HEIGHT $WIDTH $CHOICE_HEIGHT \
		"${OPTIONS[@]}" \
		2>&1 >/dev/tty)

clear
case $CHOICE in 
	1)
		echo "Your choose Option 1"
		;;
	2)
		echo "You choose Option 2"
		;;
	3)
		echo "Darth Vader came to your House."
		ls 
		echo "He looked at your files"
		;;
esac

