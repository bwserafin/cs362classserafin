#include <stdio.h>
#include <stdlib.h>

/*
*
*   This is a c program that does something cool!
*
*/

int main(int argc,	// Number of things passed into this function.
	 char *argv[]	// this is the array of things passed.
	){
    printf("=========\n"); // This is a single line comment.
    printf("Hello    \n");
    printf("=========\n\n");
    return 0;
}
