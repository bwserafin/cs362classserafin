#!/bin/bash
echo "Latex Compile Report" > LatexCompileReport.txt

for i in *.tex;
    do
	pdflatex &> /dev/null $i
    done

echo "Done Compiling"

for i in *.pdf;
    do
	FILE=$(pdftotext $i - | wc -w)
	echo $i Word Count: $FILE >> LatexCompileReport.txt
    done

rm *.aux *.log
