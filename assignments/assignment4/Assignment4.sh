#!/bin/bash

HEIGHT=15
WIDTH=40
CHOICE_HEIGHT=4
BACKTITLE="Brandon Serafin - Assignment 4"
TITLE="Assignment 4"
MENU="Please Choose an Option"

OPTIONS=(1 "Generate and Open PDF"
	 2 "Edit Assignment4.tex"
	 3 "Delete Files (.pdf, .aux, .log)"
	 4 "Exit")

CHOICE=$(dialog --clear  \
		--backtitle "$BACKTITLE" \
		--title "$TITLE" \
		--menu "$MENU" \
		$HEIGHT $WIDTH $CHOICE_HEIGHT \
		"${OPTIONS[@]}" \
		2>&1 >/dev/tty)

clear
case $CHOICE in
	1)
		pdflatex Assignment4.tex
		evince Assignment4.pdf
		./Assignment4.sh
		;;
	2)
		nano Assignment4.tex
		./Assignment4.sh
		;;
	3)
		rm Assignment4.log Assignment4.aux Assignment4.pdf
		echo "Files Removed: Assignment4.log, Assignment4.aux, Assignment4.pdf"
		echo "Press any key to continue"
		read -n 1 -s
		./Assignment4.sh
		;;

	4)
		echo "You exited the dialog"
		;;
esac

