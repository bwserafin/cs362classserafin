import os
import re
hour = [0]
minute = [0]
totalHours = 0
totalMinutes = 0
name = ""
wfName = "WeeklyReport.tex"

def findFile():
# ======================================
# Read all files in current directory
# ======================================
    for filename in os.listdir(os.getcwd()):
        x = filename.split(".")
    # ============================================
    # Find all files with the extenstion '.txt'
    # ============================================
        if x[1] == "txt":
            del hour[:]
            del minute[:]
            file = open(filename,"r")
            splitLines(file)
            file.close()
        file = open(wfName,"a")
    file.write("\end{document}")
    file.close()









def splitLines(file):
# ==================================================
# Split each line into sections. Split on a space
# ==================================================
    for line in file.readlines():
        s = line.split()
        if len(s) == 2:
            global name
            name = s[0] + " " + s[1]
        if len(s) > 2:
# ==================================================================
# If the line segment contains "hr", split and put time into array
# ==================================================================
            if "hr" in s[1]:
                nums = re.findall('\d+', s[1])
                number = nums[0]
                if number.isdigit():
                    hour.append(number)
# ==================================================================
# If the line segment contains "min", split and put time into array
# ==================================================================
            if "min" in s[2]:
                nums = re.findall('\d+', s[2])
                number = nums[0]
                if number.isdigit():
                    minute.append(number)
    addToReport(hour, minute)



def addToReport(hours, minutes):
# ======================================
# Add up all times for employee
# ======================================
    global totalHours
    global totalMinutes
    totalHours = 0
    totalMinutes = 0
    for i in hours:
        totalHours += int(i)
    for j in minutes:
        totalMinutes += int(j)
    minutesToHours()
    writeWeeklyReport(totalHours, totalMinutes)


# ======================================
# Write employee weekly info to report
# ======================================
def writeWeeklyReport(totalHours, totalMinutes):
    writeFile = open(wfName,"a")
    tHours = str(totalHours)
    tMinutes = str(totalMinutes)
    writeFile.write(name + ":\n")
    writeFile.write("\\vskip.1in")
    writeFile.write("\hskip1in Hours: "+ tHours + "\n")
    writeFile.write("\\vskip.1in")
    writeFile.write("\hskip1in Minutes:  "+ tMinutes +"\n\n")
    totalHours = 0
    totalMinutes = 0
    writeFile.close()

# =============================================================
# If minutes > 60, add 1 to hour and subtract 60 from minute
# =============================================================
def minutesToHours():
    global totalHours
    global totalMinutes
    if totalMinutes > 60:
        totalMinutes -= 60
        totalHours += 1
        minutesToHours()

# ===========================================
# Creat weekly report file (beginning info)
# ===========================================
def initialReportCreate():
    writeFile = open(wfName,"w")
    writeFile.write("\documentclass[12pt]{article}\n")
    writeFile.write("\usepackage{geometry}\n")
    writeFile.write("\geometry{hmargin={1in,1in},vmargin={2in,1in}}\n")
    writeFile.write("\\begin{document}\n\n")
    writeFile.write("Brandon Serafin - Assignment 5\n")
    writeFile.write("\\vskip.1in")
    writeFile.write("Weekly Time Report\n")
    writeFile.write("\\vskip.5in")
    writeFile.close()
