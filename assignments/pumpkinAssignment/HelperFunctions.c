/* This is a bunch of stuff to use in pixel mapping. */
#include <math.h> // Allows me to use the pow(x,y)  ---> x^y



// ####################################################################################
// Is the current pixel in the area where the orange circle (pumpkin body) should be
// ####################################################################################
int InCircle(int totalrows, int totalcols, int radius, int pixRow, int pixCol){
    int InOrOut = 0; // Integer flag for if the current pixel is in the circle or not.
                     // 1 ==> yes, 0 ==> no.
    int Centerx = totalcols/2;
    int Centery = totalrows/2;
    int dist    = 0;            //Distance from center to pixel.

    dist = pow((pow(Centerx - pixCol,2))+(pow(Centery - pixRow,2)),0.5);

    if (dist < radius){
        InOrOut = 1;
    }

    return InOrOut;
}


// ################################################################
// Is the currnet pixel in the area where the nose should be drawn
// ################################################################
int Nose(int totalrows, int totalcols, int radius, int pixRow, int pixCol){
  int NoseCorrect = 0;
  int Centerx = totalcols/2;
  int Centery = totalrows/2;
  int dist    = 0;

  dist = pow((pow(Centerx - pixCol,2))+(pow(Centery - pixRow,2)),0.5);
  int noseDist = radius * 0.1;
  if (dist < noseDist){
    NoseCorrect = 1;
  }

  return NoseCorrect;
}


// ################################################################
// Is the currnet pixel in the area where the stem should be drawn
// ################################################################
int Stem(int totalrows, int totalcols, int radius, int pixRow, int pixCol){
  int StemCorrect = 0;
  int Centerx = totalcols/2;
  int Centery = totalrows/2;
  int rowStem = Centery - radius;
  int rowLim = totalrows * 0.05;
  int colLim = (totalcols * 0.05)/2;

  int rowGood = 0;
  int colGood = 0;

  // Check if pixel's row is within the right area
  if ((pixRow >= (rowStem - rowLim)) && (pixRow <= (rowStem + rowLim))){
    rowGood = 1;
  }

  // Check if the pixel's column is within the right area
  if((pixCol >= (Centerx - colLim)) && (pixCol <= (Centerx + colLim))){
    colGood = 1;
  }

  // If both column and row are good, then the pixel is good for the stem
  if ((rowGood == 1) && (colGood == 1)){
    StemCorrect = 1;
  }
  return StemCorrect;
}



// ################################################################
// Is the currnet pixel in the area where an eye should be drawn
// ################################################################
int Eyes(int totalrows, int totalcols, int radius, int pixRow, int pixCol){
    int EyeCorrect = 0;
    int Centerx = totalcols/2;
    int Centery = totalrows/2;
    int edge = radius * 0.6;

    int rowLim = totalrows * 0.05;
    int colLim = totalcols * 0.05;
    int x = (Centerx - radius) + edge;
    int rowGood = 0;
    int colGood = 0;

    // Check if pixel's row is within the right area
    if ( (pixRow >= x) && (pixRow <= x + rowLim )){
      rowGood = 1;
    }
    // if((pixRow > 75) && (pixRow < 90)){
    //   rowGood = 1;
    // }

    // Check if the pixel's column is within the right area
    if ( ( (pixCol >= Centery - radius + edge) && (pixCol <= (Centery - radius) + edge + colLim) ) || ( (pixCol <= (Centery + radius - edge)) && (pixCol >= (Centery + radius - edge - colLim)) )  ){
      colGood = 1;
    }
    // if ( ((pixCol > 70) && (pixCol < 80)) || ((pixCol > 120) && (pixCol < 130)) ){
    //   colGood = 1;
    // }

    // If both column and row are good, then the pixel is good for an eye
    if ((rowGood == 1) && (colGood == 1)){
      EyeCorrect = 1;
    }
    return EyeCorrect;
  }

  // ################################################################
  // Is the currnet pixel in the area where the mouth should be drawn
  // ################################################################
int Mouth(int totalrows, int totalcols, int radius, int pixRow, int pixCol){
    int MouthCorrect = 0;
    int Centerx = totalcols/2;
    int Centery = totalrows/2;
    int edge = radius * 0.6;
    int rowLim = totalrows * 0.04;
    //int colLim = totalcols * 0.05;
    int x = (Centerx + radius) - edge;
    int rowGood = 0;
    int colGood = 0;

    // Check if pixel's row is within the right area
    if ( (pixRow >= x) && (pixRow <= x + rowLim )){
      rowGood = 1;
    }

    // Check if the pixel's column is within the right area
    if ( pixCol >= ((Centery - radius) + edge) && pixCol <= ((Centery + radius) - edge)){
      colGood = 1;
    }

    // If both column and row are good, then the pixel is good for the mouth
    if ((rowGood == 1) && (colGood == 1)){
      MouthCorrect = 1;
    }

    return MouthCorrect;
}
