The pumpkin should look good if(the number of rows and columns are equal && the radius of the 		circle is <= 45% of the height/width of the .ppm (i.e. if the size of the picture is 		200*200, the radius should be no bigger than 90; if 200 200 90 is used, the stem will 		touch the top of the .ppm) 

Also, the radius should be no smaller than 30 (at 30, each characteristic is present, but pressed 	closely together.)

-----------------------------------------------------------------------------------------------
To run:

sudo make
sudo ./Pumpkin pump.ppm 200 200 50  //pump, 200, 200, and 50 are all variables
