/* The header file */

/* Function to see if the value of the pixel is in the circle or not */
int InCircle(int totalrows, int totalcols, int radius, int pixRow, int pixCols);
int Nose(int totalrows, int totalcols, int radius, int pixRow, int pixCol);
int Stem(int totalrows, int totalcols, int radius, int pixRow, int pixCol);
int Eyes(int totalrows, int totalcols, int radius, int pixRow, int pixCol);
int Mouth(int totalrows, int totalcols, int radius, int pixRow, int pixCol);
